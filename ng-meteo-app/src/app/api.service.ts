import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  API_KEY = 'd346575e70d607e1108f0279b6abd5b6';
  public getWeather(ville: string){
    return this.httpClient.get(`http://api.openweathermap.org/data/2.5/weather?q=${ville}&appid=${this.API_KEY}`);
  }
  
  constructor(private httpClient: HttpClient) { }
}
