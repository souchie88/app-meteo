import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-detail-ville',
  templateUrl: './detail-ville.component.html',
  styleUrls: ['./detail-ville.component.css']
})
export class DetailVilleComponent implements OnInit {
  cityId: string = '';
  cityImageUrl: string = '';
  city: any;

  constructor(private route: ActivatedRoute, private router: Router, private http: HttpClient) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.cityId = params['id'];
      this.fetchCityDetails();
    });
    this.route.queryParams.subscribe(params => {
      this.cityImageUrl = params['imageUrl'];
      console.log(params['imageUrl'])
    });
  }

  fetchCityDetails(): void {
    const apiKey = 'd346575e70d607e1108f0279b6abd5b6';
    const apiUrl = `https://api.openweathermap.org/data/2.5/weather?id=${this.cityId}&appid=${apiKey}&units=metric`;

    this.http.get(apiUrl).subscribe((data: any) => {
      this.city = data;
    });
  }

  goBack(): void {
    this.router.navigate(['/villeListe']);
  }

  goToPrevision(cityId: string): void {
    this.router.navigate(['/prevision', cityId]);
  }
}
