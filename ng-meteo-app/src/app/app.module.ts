import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { AppComponent } from './app.component';
import { ListVilleComponent } from './list-ville/list-ville.component';
import { DetailVilleComponent } from './detail-ville/detail-ville.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { PrevisionComponent } from './prevision/prevision.component';
import { FormsModule } from '@angular/forms';
import { AccueilComponent } from './accueil/accueil.component';


const routes: Routes = [
  {
    path:'',
    component: AccueilComponent
  },
  {
    path:'accueil',
    component: AccueilComponent
  },
  {
    path:'villeListe',
    component: ListVilleComponent
  },
  {
    path:'villeListe/:id',
    component: DetailVilleComponent
  },
  {
    path:'prevision/:id',
    component: PrevisionComponent
  },
  {
    path:'**', // '**' = les routes non trouvées
    component: PageNotFoundComponent
  }
];

@NgModule({
  declarations: [
    AppComponent,
    DetailVilleComponent,
    ListVilleComponent,
    PrevisionComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
