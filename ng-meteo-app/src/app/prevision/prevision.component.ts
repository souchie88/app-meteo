import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-prevision',
  templateUrl: './prevision.component.html',
  styleUrls: ['./prevision.component.css']
})
export class PrevisionComponent implements OnInit {
  cityId: string = '';
  forecasts: any[] = [];
  previousCityId: string = ''; // Variable pour stocker l'ID de la ville précédente

  imageUrls: { [key: string]: string } = {
    'light rain': 'https://images.unsplash.com/uploads/14122598319144c6eac10/5f8e7ade?q=80&w=1961&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D',
    'clear sky': 'https://media.istockphoto.com/id/673343564/fr/photo/ciel.jpg?s=2048x2048&w=is&k=20&c=mRtaJIzeI8cirEdPPe3OewfxOiZwDP2upgbzrQlJQtg=',
    'heavy intensity rain': 'https://images.unsplash.com/photo-1513728731559-6bf2c0c931f9?q=80&w=2070&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D',
    'overcast clouds' : 'https://plus.unsplash.com/premium_photo-1674331863328-78a318c57447?q=80&w=2072&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D',
    'broken clouds' : 'https://plus.unsplash.com/premium_photo-1674331863328-78a318c57447?q=80&w=2072&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D',
    'scattered clouds' : 'https://plus.unsplash.com/premium_photo-1674331863328-78a318c57447?q=80&w=2072&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D',
    'few clouds' : 'https://plus.unsplash.com/premium_photo-1674331863328-78a318c57447?q=80&w=2072&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D',
    'moderate rain' : 'https://images.unsplash.com/uploads/14122598319144c6eac10/5f8e7ade?q=80&w=1961&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D',
  };


  constructor(private route: ActivatedRoute, private router: Router, private http: HttpClient) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.cityId = params['id'];
      this.fetchForecasts();
    });
  }

  fetchForecasts(): void {
    // Stocker l'ID de la ville précédente
    this.previousCityId = this.cityId;

    const apiKey = 'd346575e70d607e1108f0279b6abd5b6';
    const apiUrl = `https://api.openweathermap.org/data/2.5/forecast?id=${this.cityId}&appid=${apiKey}&units=metric`;

    this.http.get(apiUrl).subscribe((data: any) => {
      // Group forecasts by day
      const groupedForecasts: any = {};
      data.list.forEach((forecast: any) => {
        const date = new Date(forecast.dt_txt.split(' ')[0]);
        const day = date.toDateString();
        if (!groupedForecasts[day]) {
          groupedForecasts[day] = forecast;
        }
      });

      // Convert object back to array
      this.forecasts = Object.values(groupedForecasts);
    });
  }

  // Méthode pour revenir à la page de détail de la ville précédente
  goBack(): void {
    this.router.navigate(['/villeListe', this.previousCityId]); // Redirection vers la page de détail de la ville précédente
  }
}
